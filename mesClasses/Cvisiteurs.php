<?php

/* * *********** Classe métier Cvisiteur et Classe de contrôle Cvisiteurs **************** */
require_once 'mesClasses/Ctri.php';
require_once 'mesClasses/Cdao.php';

// Odabas Bulent
class Cemploye 
{

    public $id;
    public $login;
    public $nom;
    public $prenom;
    public $mdp;
    public $mdp_hash;
    public $ville;

    function __construct($sid, $slogin, $snom, $sprenom, $mdp, $smdp_hash, $sville) { //s pour send param envoyé

        $this->id = $sid;
        $this->login = $slogin;
        $this->nom = $snom;
        $this->prenom = $sprenom;
        $this->mdp = $mdp;
        $this->mdp_hash = $smdp_hash;
        $this->ville = $sville;
    }

}

Class Cvisiteur extends Cemploye // hérite de Cemploye
{

    function __construct($sid, $slogin, $snom, $sprenom, $mdp, $smdp_hash, $sville) {
        parent :: __construct($sid, $slogin, $snom, $sprenom, $mdp, $smdp_hash, $sville); //take the constructor also, donc no need de le refaire
    }

}

class Cvisiteurs {

    public $ocollVisiteurById;
    private $ocollVisiteurByLogin;
    private $ocollVisiteur;

    public function __construct() {

        try {
            $query = "SELECT * FROM visiteur";
            $odao = new Cdao();
            $lesVisiteurs = $odao->gettabDataFromSql($query);

            foreach ($lesVisiteurs as $unVisiteur) {
                $ovisiteur = new Cvisiteur($unVisiteur['id'], $unVisiteur['login'], $unVisiteur['nom'], $unVisiteur['prenom'], $unVisiteur['mdp'], $unVisiteur['mdp_hash'], $unVisiteur['ville']);
                $this->ocollVisiteur[] = $ovisiteur;
                $this->ocollVisiteurById[$ovisiteur->id] = $ovisiteur;
                $this->ocollVisiteurByLogin[$ovisiteur->login] = $ovisiteur;
            }
        } catch (PDOException $e) {
            $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
            die($msg);
        }
    }

    //ici les méthodes à rajouter



    function getVisiteurById($sid) {
        if (array_key_exists($sid, $this->ocollVisiteurById)) {
            $ovisiteur = $this->ocollVisiteurById[$sid];
            return $ovisiteur;
        }
    }

    function getVisiteurByLogin($login) {
        if (array_key_exists($login, $this->ocollVisiteurByLogin)) {
            $ovisiteur = $this->ocollVisiteurByLogin[$login];
            return $ovisiteur;
        }
    }

    function verifierInfosConnexion($username, $spwd) {
        $mdp_hash_recalc = hash('sha512', $spwd);
        foreach ($this->ocollVisiteurById as $ovisiteur) {
            if ($ovisiteur->login == $username && $ovisiteur->mdp_hash == $mdp_hash_recalc) {
                return $ovisiteur;
            }
        }
        return null;
    }

    public function getVisiteursTrie($attribut) {
        $otrie = new Ctri();
        $ocollVisiteursTrie = $otrie->TriTableau($this->ocollVisiteur, $attribut);

        return $ocollVisiteursTrie;
    }

    function getTabVisiteursParNomEtVille($sdebutFin, $spartieNom, $sville) {
        $tabVisiteursByVilleNom = null;
        /* if($partieNom == '*' && $sville == 'toutes')
          {
          return $this->ocollVisiteur;
          } */
        foreach ($this->ocollVisiteur as $ovisiteur) {
            if ((strtolower($ovisiteur->ville) == strtolower($sville)) || $sville == 'toutes') {
                if ($spartieNom != '*') {
                    if ($sdebutFin == "debut") {
                        $nomExtrait = substr($ovisiteur->nom, 0, strlen($spartieNom));

                        if (strtolower($nomExtrait) == strtolower($spartieNom)) {
                            $tabVisiteursByVilleNom[] = $ovisiteur;
                        }
                    }
                    if ($sdebutFin == "fin") {

                        $nomExtrait = substr($ovisiteur->nom, -strlen($spartieNom), strlen($spartieNom));

                        if (strtolower($nomExtrait) == strtolower($spartieNom)) {
                            $tabVisiteursByVilleNom[] = $ovisiteur;
                        }
                    }

                    if ($sdebutFin == "nimporte") {
                        $i = 0;
                        $tab = str_split($ovisiteur->nom);
                        foreach ($tab as $caract) {
                            $nomExtrait = substr($ovisiteur->nom, $i, strlen($spartieNom));

                            if (strtolower($nomExtrait) == strtolower($spartieNom)) {
                                $tabVisiteursByVilleNom[] = $ovisiteur;
                                break;
                            }

                            $i++;
                        }
                    }
                } else {
                    $tabVisiteursByVilleNom[] = $ovisiteur;
                }
            }
        }

        return $tabVisiteursByVilleNom;
    }

}
